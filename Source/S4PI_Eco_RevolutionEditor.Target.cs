// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class S4PI_Eco_RevolutionEditorTarget : TargetRules
{
	public S4PI_Eco_RevolutionEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("S4PI_Eco_Revolution");
	}
}
