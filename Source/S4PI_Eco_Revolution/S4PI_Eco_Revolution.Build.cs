// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class S4PI_Eco_Revolution : ModuleRules
{
	public S4PI_Eco_Revolution(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
