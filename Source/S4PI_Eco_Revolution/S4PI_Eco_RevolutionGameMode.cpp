// Copyright Epic Games, Inc. All Rights Reserved.

#include "S4PI_Eco_RevolutionGameMode.h"
#include "S4PI_Eco_RevolutionCharacter.h"
#include "UObject/ConstructorHelpers.h"

AS4PI_Eco_RevolutionGameMode::AS4PI_Eco_RevolutionGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
