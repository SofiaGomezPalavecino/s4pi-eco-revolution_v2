// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "S4PI_Eco_RevolutionGameMode.generated.h"

UCLASS(minimalapi)
class AS4PI_Eco_RevolutionGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AS4PI_Eco_RevolutionGameMode();
};



