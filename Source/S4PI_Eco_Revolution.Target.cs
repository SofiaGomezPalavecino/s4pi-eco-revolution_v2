// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class S4PI_Eco_RevolutionTarget : TargetRules
{
	public S4PI_Eco_RevolutionTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("S4PI_Eco_Revolution");
	}
}
